###Ruby 2.1

You'll need the epel repo
```
curl -O http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
```
```
rpm -Uvh epel-release-6-8.noarch.rpm
```


```
yum -y groupinstall "Development Tools" 
```
```
yum -y install libyaml libyaml-devel readline-devel ncurses-devel gdbm-devel tcl-devel openssl-devel db4-devel libffi-devel
```


####make a dir
```
mkdir -p rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS} 
```

####get the source 
```
curl -O https://bitbucket.org/wasmum/rpms/src/d4444b6bcd81940b8064add95a22b1564126c066/ruby-2.1.0.tar.gz -P rpmbuild/SOURCES
``` 

####get the spec
```
curl -O https://bitbucket.org/wasmum/rpms/src/d4444b6bcd81940b8064add95a22b1564126c066/ruby21.spec -P rpmbuild/SPECS
```

####build 
```
rpmbuild -bb rpmbuild/SPECS/ruby21.spec
```

####install
```
rpm -Uhv rpmbuild/RPMS/x86_64/ruby-2.1.0-1.el6.x86_64.rpm
```	
